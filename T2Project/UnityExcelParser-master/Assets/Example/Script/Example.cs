﻿using UnityEngine;
using System.Collections;

public class Example : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UserInfoMgr.instance.InitData();

		UserInfoBean uib = UserInfoMgr.instance.GetDataById(1);
		Debug.Log(uib.Name);
        UserInfoBean uib2 = UserInfoMgr.instance.GetDataById(2);
        Debug.Log(uib2.Name);
        Debug.Log(uib2.Type);
        UserInfoMgr.instance.Test();

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
