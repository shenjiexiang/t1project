using UnityEngine;
using System.Collections;
using ExcelParser;

public class UserInfo00Bean : IDataBean {
 
 

	private int id;
	public int Id {
		get {
			return id;
		}
		set {
			id = value;
		}
	}
 

	private string name;
	public string Name {
		get {
			return name;
		}
		set {
			name = value;
		}
	}
 

	private int type;
	public int Type {
		get {
			return type;
		}
		set {
			type = value;
		}
	}
 
}
