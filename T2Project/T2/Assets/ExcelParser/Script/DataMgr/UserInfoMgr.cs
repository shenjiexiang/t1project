using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ExcelParser;

public partial class UserInfoMgr : DataMgrBase<UserInfoMgr> {


	protected override string GetXlsxPath ()
	{
		return "UserInfo00";
	}


	protected override System.Type GetBeanType ()
	{
		return typeof(UserInfoBean);
	}

    public GameObject FindMark(int id)
    {
        if (id > 0 && id <= 2)
        {
           return GameObject.Find("Maker1GameRootObj(Clone)");
        }
        return null;
    }

	public UserInfoBean GetDataById(object id)
	{
		IDataBean dataBean = _GetDataById(id);

		if(dataBean!=null)
		{
			return (UserInfoBean)dataBean;
		}else{
			return null;
		}
	}



}
