using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using ExcelParser;

public partial class UserInfo00Mgr : DataMgrBase<UserInfo00Mgr> {


	protected override string GetXlsxPath ()
	{
		return "UserInfo00";
	}


	protected override System.Type GetBeanType ()
	{
		return typeof(UserInfo00Bean);
	}


	public UserInfo00Bean GetDataById(object id)
	{
		IDataBean dataBean = _GetDataById(id);

		if(dataBean!=null)
		{
			return (UserInfo00Bean)dataBean;
		}else{
			return null;
		}
	}



}
