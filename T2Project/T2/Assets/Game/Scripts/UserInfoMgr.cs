﻿using UnityEngine;
using System.Collections;


public partial class UserInfoMgr
{
    public string GetMarkerName(MarkerUI_Default maker)
    {
        return maker.uiName;
    }
    public MarkerUI_Default MakerChecker(string MarkerName, MarkerUI_Default _maker)
    {
        if (GameObject.Find(MarkerName) == null)
        {
            return null;
        }
        else
        {
            return GameObject.Find(MarkerName).GetComponent<MarkerUI_Default>();
        }
    }
}

