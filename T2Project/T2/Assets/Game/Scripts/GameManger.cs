﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManger : MonoBehaviour
{
    public MarkerUI_Default _maker1, _maker2, _maker3;
    private MakerState _makerState;
    private static Text nodeInformation;

    public static Text NodeInformation { get => nodeInformation; set => nodeInformation = value; }

    // Start is called before the first frame update
    void Start()
    {
        UserInfoMgr.instance.InitData();
        NodeInformation = GameObject.Find("TextToShow").GetComponent<Text>();
        NodeInformation.text = "请选择一个节点";
        ShowAllNode();
    }

    private void ShowAllNode()
    {

    }

    // Update is called once per frame
    void Update()
    {
        CheckWhichMarkerIn();
    }
    public void CheckWhichMarkerIn()
    {
        if (GameObject.FindObjectOfType<MarkerUI_Default>() != null)
        {
            if (GameObject.FindObjectOfType<MarkerUI_Default>().uiName == UserInfoMgr.instance.GetMarkerName(_maker1))
            {
                //生成maker1的节点
                Debug.Log("生成maker1的节点");
                _makerState = MakerState.maker1;
                if (GameObject.Find("Maker1GameRootObj(Clone)") != null)
                {
                    _makerState = MakerState.Idel;
                }


            }
            if (GameObject.FindObjectOfType<MarkerUI_Default>().uiName == UserInfoMgr.instance.GetMarkerName(_maker2))
            {
                //生成maker2的节点
                Debug.Log("生成maker2的节点");
            }
            if (GameObject.FindObjectOfType<MarkerUI_Default>().uiName == UserInfoMgr.instance.GetMarkerName(_maker3))
            {
                //生成maker3的节点
                Debug.Log("生成maker3的节点");
            }
        }
        else
        {

        }
        switch (_makerState)
        {
            case MakerState.None:
                ShowAllNode();
                break;
            case MakerState.maker1:
                Instantiate(Resources.Load("MarkerGameROOT/Maker1GameRootObj", typeof(GameObject)));
                if (GameObject.Find("Maker1GameRootObj(Clone)") != null)
                {
                    for (int i = 1; i <= 2; i++)
                    {

                        Instantiate(Resources.Load("NodeGameROOT/" + i.ToString(), typeof(GameObject)));
                    }
                }
                break;

        }
    }

}
public enum MakerState
{
    Idel,
    None,
    maker1,
    maker2,
    maker3,
    maker12,
    maker23,
    maker13,
}


