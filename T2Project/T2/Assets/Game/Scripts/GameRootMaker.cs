﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRootMaker : MonoBehaviour
{
    public MarkerUI_Default _maker;
    // Start is called before the first frame update
    void Start()
    {

        UserInfoMgr.instance.InitData();
        MarkChecker();
    }

    // Update is called once per frame
    void Update()
    {

        if (_maker != null)
        {
            transform.position = Camera.main.ScreenToWorldPoint(_maker.transform.position);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    private void MarkChecker()
    {
        if (_maker == null)
        {
            switch (transform.name)
            {
                case "Maker1GameRootObj(Clone)":
                    _maker = UserInfoMgr.instance.MakerChecker("MarkerUI_1", _maker);
                   
                    break;
                case "Maker2GameRootObj(Clone)":
                    _maker = UserInfoMgr.instance.MakerChecker("MarkerUI_2", _maker);
                   
                    break;
                case "Maker3GameRootObj(Clone)":
                    _maker = UserInfoMgr.instance.MakerChecker("MarkerUI_3", _maker);
                   
                    break;
            }


        }
        else
        {
            transform.position = Camera.main.ScreenToWorldPoint(_maker.transform.position);
        }
       
    }
}
