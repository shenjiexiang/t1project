﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRootNode : MonoBehaviour
{
    public UserInfoBean _uib;
    public int _id;
    public GameObject _targetMark;
    private LineRenderer _lineRenderer;
    private void OnEnable()
    {
        UserInfoMgr.instance.InitData();
        _uib = UserInfoMgr.instance.GetDataById(_id);
        _targetMark= UserInfoMgr.instance.FindMark(_id);
        _lineRenderer = transform.GetComponent<LineRenderer>();
        transform.GetComponent<SpringJoint2D>().connectedBody = _targetMark.GetComponent<Rigidbody2D>();
        transform.position = new Vector3(_targetMark.transform.position.x-UnityEngine.Random.Range(0, 3), _targetMark.transform.position.y+UnityEngine.Random.Range(0,3), -10.3f);


    }



    // Update is called once per frame
    void Update()
    {
        if (_targetMark == null)
        {
            Destroy(gameObject);
        }
        else
        {
            _lineRenderer.SetPosition(0, transform.position);
            _lineRenderer.SetPosition(1, _targetMark.transform.position);
        }
      
    }

    private void OnMouseDown()
    {
        GameManger.NodeInformation.text = _uib.Name+_uib.Content;
    }


}
