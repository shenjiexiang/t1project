﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoPlayerForT1 : MonoBehaviour
{
    public static Text _text;
    public static RawImage _rawImage;
    public static VideoPlayer _videoPayer1;

    protected AudioSource _audioSource;
    TuioListener TL;

    Animator m_Animator;
    string m_ClipName;
    AnimatorClipInfo[] m_CurrentClipInfo;


    void Awake()
    {
        InitGame();
    }
    private void InitGame()
    {
        FitScreenSize();
        InputInit();
        EventSystem.Instance.Register(FrameEventId.OnStartGame, HandleEvent);
        EventSystem.Instance.Register(FrameEventId.OnCheckZone1, HandleEvent);
        EventSystem.Instance.Register(FrameEventId.OnCheckZone2, HandleEvent);
        EventSystem.Instance.Register(FrameEventId.OnWrongPlace, HandleEvent);
        _text = GameObject.Find("TextWarring").GetComponent<Text>();
        showText("开始讲解");
        //InvokeRepeating("checkedTouch1", 0, 1f);

    }
    public static bool checkedTouch1()
    {
        if (GameObject.FindGameObjectWithTag("Touch01"))
        {
            return true;
        }
        else
        { return false; }
    }
    public static void showText(string warring)
    {
        _text.text = warring;
    }
    // Start is called before the first frame update
    void Start()
    {


        // EventSystem.Instance.Send(FrameEventId.OnCheckZone2);
        EventSystem.Instance.Send(FrameEventId.OnStartGame);



    }
    private void HandleEvent(int key, object[] param)
    {
        switch (key)
        {
            case (int)FrameEventId.OnStartGame:
                Debug.Log("播放Idel动画");
                StartCoroutine(Play("VP1"));
                break;
            case (int)FrameEventId.OnCheckZone1:
                Debug.Log("播放1动画");
                StartCoroutine(Play("VP2"));
                break;
            case (int)FrameEventId.OnCheckZone2:
                Debug.Log("播放2动画");
                StartCoroutine(Play("VP3"));
                break;
            case (int)FrameEventId.OnWrongPlace:
                Debug.Log("播放3动画");
                EventSystem.Instance.UnRegister(FrameEventId.OnCheckZone1, HandleEvent);
                break;

        }
    }



    private void InputInit()
    {
        TL = GameObject.FindObjectOfType<TuioListener>();
        if (!TL)
        {
            Debug.LogError("场景中需要输入监听");
        }
    }

    private void Update()
    {

        if (VideoPlayerForT1.checkedTouch1() == false)
        {
            TouchUI1.unlockFindalVideo = false;
            EventSystem.Instance.Send(FrameEventId.OnStartGame);
        }

    }
    public IEnumerator Play(string animationName)
    {
        _videoPayer1.clip = Resources.Load<VideoClip>(animationName);
        _videoPayer1.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1);
        while (!_videoPayer1.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }
        _rawImage.texture = _videoPayer1.texture;
        _videoPayer1.Play();

    }



    public void FitScreenSize()
    {
        _rawImage = Resources.Load<RawImage>("VP");
        Instantiate(_rawImage, transform);


        transform.GetChild(0).GetComponent<RectTransform>().sizeDelta = new Vector2(Screen.width, Screen.height);
        _rawImage = transform.GetChild(0).GetComponent<RawImage>();
        _videoPayer1 = FindObjectOfType<VideoPlayer>();
        _rawImage.texture = _videoPayer1.texture;

    }


}
