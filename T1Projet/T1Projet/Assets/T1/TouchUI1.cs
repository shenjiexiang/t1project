﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class TouchUI1 : BaseMarkerUI
{
    public Text codeText;
    public Image bodyImg;

    public RectTransform checkzon1;
    public RectTransform checkzon2;
    private float MaxX, MaxY, MinX, MinY;
    private float MaxX2, MaxY2, MinX2, MinY2;
    private float distance, totalDistance;
    public static bool unlockFindalVideo;
    //根据marker参数更新ui
    public override void UpdateUI()
    {
        if (mMarker == null || codeText == null || bodyImg == null)
        {
            Debug.LogError("Update Marker UI Error!");
            return;
        }
        codeText.text = mMarker.code.ToString();
        transform.position = mMarker.position;
        bodyImg.transform.rotation = Quaternion.Euler(0, 0, -mMarker.angle);
    }
    private void Start()
    {
        if (checkzon1 == null)
        {
            checkzon1 = GameObject.FindGameObjectWithTag("checkZone01").GetComponent<RectTransform>();
        }
        if (checkzon2 == null)
        {
            checkzon2 = GameObject.FindGameObjectWithTag("checkZone02").GetComponent<RectTransform>();
        }
        MaxX = checkzon1.position.x + (checkzon1.sizeDelta.x / 2);
        MinX = checkzon1.position.x - (checkzon1.sizeDelta.x / 2);
        MaxY = checkzon1.position.y + (checkzon1.sizeDelta.y / 2);
        MinY = checkzon1.position.y - (checkzon1.sizeDelta.y / 2);

        MaxX2 = checkzon2.position.x + (checkzon2.sizeDelta.x / 2);
        MinX2 = checkzon2.position.x - (checkzon2.sizeDelta.x / 2);
        MaxY2 = checkzon2.position.y + (checkzon2.sizeDelta.y / 2);
        MinY2 = checkzon2.position.y - (checkzon2.sizeDelta.y / 2);
        unlockFindalVideo = false;

    }
    private void Update()
    {

        if (transform.position.x < MaxX && transform.position.x > MinX && transform.position.y < MaxY && transform.position.y > MinY)
        {
            Debug.Log("正确");
            VideoPlayerForT1.showText("请收听讲解");
            EventSystem.Instance.Send(FrameEventId.OnCheckZone1);
            distance = Vector3.Distance(transform.position, new Vector3(checkzon1.position.x, MinY, 0));
            totalDistance = Vector3.Distance(new Vector3(checkzon1.position.x, MinY, 0), checkzon2.position);
            VideoPlayerForT1._videoPayer1.playbackSpeed = Mathf.Abs(distance / totalDistance) * 10;
            unlockFindalVideo = true;

        }
        else if (transform.position.x < MaxX2 && transform.position.x > MinX2 && transform.position.y < MaxY2 && transform.position.y > MinY2 && unlockFindalVideo == true)
        {
            Debug.Log("正确2");
            VideoPlayerForT1.showText("请收听讲解2");
            VideoPlayerForT1._videoPayer1.playbackSpeed = 1;
            EventSystem.Instance.Send(FrameEventId.OnCheckZone2);

        }
        else if (unlockFindalVideo == true)
        {
            Debug.Log("错误");


            EventSystem.Instance.Send(FrameEventId.OnStartGame);
            VideoPlayerForT1._videoPayer1.playbackSpeed = 1;

            if (transform.position.x > MaxX)
            {
                Debug.Log("请将令牌左移，放入卡槽中");
                VideoPlayerForT1.showText("请将令牌左移，放入卡槽中");
            }
            if (transform.position.x < MinX)
            {
                Debug.Log("请将令牌右移，放入卡槽中");
                VideoPlayerForT1.showText("请将令牌右移，放入卡槽中");
            }
            if (transform.position.y > MaxY)
            {

                Debug.Log("请将令牌下移，放入卡槽中");
                VideoPlayerForT1.showText("请将令牌下移，放入卡槽中");
            }
            if (transform.position.y < MinY)
            {
                Debug.Log("请将令牌上移，放入卡槽中");
                VideoPlayerForT1.showText("请将令牌上移，放入卡槽中");
            }
        }



    }

}
