﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Example1 : MonoBehaviour {


    //退出程序
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}
